<?php

/**
 * Plugin Name:       Toy Gist
 * Plugin URI:        https://gitlab.com/67656e65726963/toy-gist
 * Description:       Helps source gists.
 * Version:           0.0.0
 * Author:            67656e65726963
 * Author URI:        https://gitlab.com/67656e65726963
 * License:           Unlicense
 * License URI:       http://unlicense.org/
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

add_shortcode(
	'gist',
	function( $atts, $path = '' ) {
		$text = '';

		if ( ! $path ) {
			if ( isset( $atts['path'] ) ) {
				$path = wp_strip_all_tags( $atts['path'] );
			}

			if ( isset( $atts[0] ) ) {
				$path = $atts[0];
			}
		}

		if ( isset( $path ) ) {
			$text = wp_remote_get( 'https://gist.githubusercontent.com/' . $path . '/raw' );
		}

		return $text;
	},
	67
);

add_action(
	'admin_print_footer_scripts',
	function() {
		if ( wp_script_is( 'quicktags' ) ) : ?>
			<script type="text/javascript">
				QTags.addButton( 'eg_gist', 'gist', '[gist]', '[/gist]', 'gist', 'Gist', 201 );
			</script>
			<?php
		endif;
	},
	67
);
