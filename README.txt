=== Toy Gist ===
Contributors: thewhodidthis
Tags: github, gist, shortcode
Requires at least: 3.0.1
Tested up to: 5.5
Stable tag: 0.0
License: Unlicense
License URI: http://unlicense.org/

Helps source raw gist content.

```php
<?php

// Prints out 'coucou'
echo do_shortcode('[gist thewhodidthis/2db8b81ca0ee930ddccf9748ee477dc4]');
?>
```
